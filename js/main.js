$('.trigger-hide').on('click', function(){
  $('aside').addClass("hidden");
  $('article').addClass('full');
  $('.trigger-show').show();
  $('.trigger-hide').hide();
});

$('.trigger-show').on('click', function(){
  $('aside').removeClass("hidden");
  $('article').removeClass('full');
  $('.trigger-show').hide();
  $('.trigger-hide').show();
});

$(window).bind("load", function() { 
      
       var footerHeight = 0,
           footerTop = 0,
           $footer = $("footer");
           
       positionFooter();
       
       function positionFooter() {
       
                footerHeight = $footer.height();
                footerTop = ($(window).scrollTop()+$(window).height()-footerHeight)+"px";
       
               if ( ($(document.body).height()+footerHeight) < $(window).height()) {
                   $footer.css({
                        position: "absolute"
                   }).animate({
                        top: footerTop
                   })
               } else {
                   $footer.css({
                        position: "static"
                   })
               }
               
       }

       $(window)
               .scroll(positionFooter)
               .resize(positionFooter)
               
});
